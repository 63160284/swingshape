/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.swingshape;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Thanya
 */
public class TriangleFrame {
  public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(400, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);
        
        JLabel lblBase = new JLabel("base: ", JLabel.TRAILING);
        lblBase.setSize(45, 20);
        lblBase.setLocation(5, 5);
        lblBase.setBackground(Color.WHITE);
        lblBase.setOpaque(true);
        frame.add(lblBase);

        JTextField txtBase = new JTextField();
        txtBase.setSize(50, 20);
        txtBase.setLocation(55, 5);
        frame.add(txtBase);
        
        JLabel lblHeight = new JLabel("height: ", JLabel.TRAILING);
        lblHeight.setSize(45, 20);
        lblHeight.setLocation(5, 25);
        lblHeight.setBackground(Color.WHITE);
        lblHeight.setOpaque(true);
        frame.add(lblHeight);

        JTextField txtHeight = new JTextField();
        txtHeight.setSize(50, 20);
        txtHeight.setLocation(55, 25);
        frame.add(txtHeight);
        
        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(115, 5);
        frame.add(btnCalculate);
        
        final JLabel lblResult = new JLabel("Rectangle base = ?? height = ?? area = ??");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(400, 50);
        lblResult.setLocation(0, 100);
        lblResult.setBackground(Color.YELLOW);
        lblResult.setOpaque(true);
        frame.add(lblResult);

        btnCalculate.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              try{
                String strBase = txtBase.getText();
                String strHeight = txtHeight.getText();
                double base = Double.parseDouble(strBase);
                double height = Double.parseDouble(strHeight);
                Triangle triangle = new Triangle(base,height);
                lblResult.setText("Triangle b = "+String.format("%.2f",triangle.getBase())
                +" h = "+String.format("%.2f",triangle.getHeight())+" area = "+String.format("%.2f",triangle.calArea()));
              } catch(Exception ex){
                  JOptionPane.showMessageDialog(frame,"Error: Please input number"
                          ,"Error",JOptionPane.ERROR_MESSAGE);
                  txtBase.setText("");
                  txtHeight.setText("");
                  txtBase.requestFocus();
                  txtHeight.requestFocus();
              }
            }
        });
        
        frame.setVisible(true);
    }
}
